$(document).ready(function(){
	"use strict";
	$(".vertical_menu").on("click",function(){
		if(window.innerWidth<992){
			$(this).find('.submenu-vertical').stop().slideToggle(500);
		}else{
			return false;
		}
	});
	$(window).resize(function(){
		if(window.innerWidth<992){
			$('.submenu-vertical').hide();
		}else{
			$('.submenu-vertical').show();
		}
	});
	$(".menu-icon-mobie").on("click",function(){
		  $("#wrap-sidebar").css({"width":"250px"});
		  $("body").css({"background-color":"rgba(0,0,0,0.4)"});

	});
	$(".closebtn").on("click",function(){
		  $("#wrap-sidebar").css({"width":"0px"});
		  $("body").css({"background-color":"white"});
	});
	$(document).mouseup(function (e)
    {
	  var container = $("#wrap-sidebar"); // YOUR CONTAINER SELECTOR
	  var close_container = $(".menu-icon-mobie");
	  if ((!container.is(e.target)  && container.has(e.target).length === 0) && (!close_container.is(e.target)  && close_container.has(e.target).length === 0))
	  {
	    container.css({"width":"0px"});
	    $("body").css({"background-color":"white"});
	  }
	});
});
